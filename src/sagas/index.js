import { all } from 'redux-saga/effects';
import documentSaga from './documentSaga';
export default function* rootSaga() {
  yield all([
    ...documentSaga
  ]);
}