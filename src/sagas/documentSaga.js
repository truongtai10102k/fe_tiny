import * as types from "../constants";
import { put, takeEvery } from 'redux-saga/effects'
import CallAPI from "../fetchAPIs/callAPI"

function* getDocument(action) {
    try {
        const res = yield CallAPI(types.HTTP_READ, "",);
        yield put({
            type: types.GET_DOCUMENT_SUCCESS,
            payload: res
        })

    } catch (error) {
        yield put({
            type: types.GET_DOCUMENT_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}

function* postDocument(action) {
    try {
        const res = yield CallAPI(types.HTTP_CREATE, "", action.payload);
        yield put({
            type: types.POST_DOCUMENT_SUCCESS,
            payload: res
        })
        yield put({
            type: types.GET_DOCUMENT_REQUEST
        })

    } catch (error) {
        yield put({
            type: types.POST_DOCUMENT_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}

function* putDocument(action) {
    try {
        const res = yield CallAPI(types.HTTP_UPDATE, `/${action.payload.id}`, action.payload);
        yield put({
            type: types.PUT_DOCUMENT_SUCCESS,
            payload: res
        })
        yield put({
            type: types.GET_DOCUMENT_REQUEST
        })

    } catch (error) {
        yield put({
            type: types.PUT_DOCUMENT_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}
function* deleteDocument(action) {
    try {
        const res = yield CallAPI(types.HTTP_DELETE, `/${action.payload}`,);
        yield put({
            type: types.DELETE_DOCUMENT_SUCCESS,
            payload: res
        })
        yield put({
            type: types.GET_DOCUMENT_REQUEST
        })

    } catch (error) {
        yield put({
            type: types.DELETE_DOCUMENT_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}

const sagaDocument = [
    takeEvery(types.GET_DOCUMENT_REQUEST, getDocument),
    takeEvery(types.POST_DOCUMENT_REQUEST, postDocument),
    takeEvery(types.PUT_DOCUMENT_REQUEST, putDocument),
    takeEvery(types.DELETE_DOCUMENT_REQUEST, deleteDocument),
]
export default sagaDocument