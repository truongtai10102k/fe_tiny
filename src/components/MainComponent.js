import React, { useState } from 'react';
import TinyComponent from './TinyComponent';
import ViewComponent from './ViewComponent';

const MainComponent = (props) => {
    const [item, setItem] = useState("");
    const [itemView, setItemView] = useState("");
    let { listItems, deleteDocument } = props;
    let arrItem = []
    if (listItems && listItems.length > 0) {
        arrItem = listItems.map((item, idx) => {
            return (
                <tr key={idx}>
                    <th>{idx + 1}</th>
                    <th>{item.title}</th>
                    <th><button onClick={() => {
                        setItem(item)
                    }}>Sửa</button></th>
                    <th><button onClick={() => {
                        deleteDocument(item._id)
                    }} >Xóa</button></th>
                    <th><button onClick={() => {
                        setItemView(item)
                    }} >Xem</button></th>
                </tr>
            )
        })
    }


    return (
        <div>
            <div>
                <tr>
                    <th>STT</th>
                    <th>name</th>
                </tr>
                {arrItem}
            </div>
            <TinyComponent {...props} item={item} />

            <ViewComponent item={itemView} />
        </div>
    );
}

export default MainComponent;
