import React, { useEffect, useState } from 'react';
import marked from 'marked';

const ViewComponent = (props) => {
    const { item } = props
    const [content, setContent] = useState("");

    useEffect(() => {
        if (item.content) {
            setContent(item.content)
        }
    }, [item]);

    const preview = (content) => {
        return { __html: marked(content) }
    }
    return (
        <div>
            <div style={{fontWeight : "bold"}}>Preview document </div>
            <div style={{fontWeight : "600"}}>Tiêu đề : {item.title} </div>
            <div dangerouslySetInnerHTML={preview(content)} />
        </div>
    );
}

export default ViewComponent;
