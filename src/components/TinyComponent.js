import React, { useRef, useState, useEffect } from 'react';
import { Editor } from '@tinymce/tinymce-react';
const TinyComponent = (props) => {
    const { postDocument, putDocument, item } = props

    const [title, setTitle] = useState("");

    useEffect(() => {
        setTitle(item.title)
    }, [item]);

    const editorRef = useRef(null);
    const postDocumentHandle = async () => {
        if (editorRef.current) {
            if (title) {
                await postDocument({ title: title, content: editorRef.current.getContent() })
            } else {
                alert("Nhập tiêu đề !!")
            }
        }
    };
    const putDocumentHandle = async () => {
        if (editorRef.current) {
            if (title) {
                await putDocument({ id: item._id, title: title, content: editorRef.current.getContent() })
            } else {
                alert("Nhập tiêu đề !!")
            }
        }
    };
    return (
        <div style={{ marginLeft: "30%" }}>
            tiêu đề: <input placeholder={"nhập title"} onChange={(e) => {
                setTitle(e.target.value)
            }} value={title} />


            <Editor
                apiKey={"3tln8xwxf5nbrw6ekyo6og56i42bf6jk8zr6nk9emuvdd0gm"}
                onInit={(evt, editor) => editorRef.current = editor}
                initialValue={item.content}
                init={{
                    height: 350,
                    width: 500,
                    menubar: false,
                    plugins: [
                        'advlist autolink lists link image charmap print preview anchor',
                        'searchreplace visualblocks code fullscreen',
                        'insertdatetime media table paste code help wordcount',
                        'image'
                    ],

                    images_upload_handler: function (blobInfo, success, failure, progress) {
                        var xhr, formData;

                        xhr = new XMLHttpRequest();
                        xhr.withCredentials = false;
                        xhr.open('POST', 'http://localhost:3001/api/editor/upload-editor')

                        xhr.upload.onprogress = function (e) {
                            progress(e.loaded / e.total * 100);
                        };

                        xhr.onload = function () {
                            var json;

                            if (xhr.status === 403) {
                                console.log("403");
                                failure('HTTP Error: ' + xhr.status, { remove: true });
                                return;
                            }

                            if (xhr.status < 200 || xhr.status >= 300) {
                                console.log("200-300");
                                failure('HTTP Error: ' + xhr.status);
                                return;
                            }
                            console.log("texttt ", xhr.responseText);
                            json = JSON.parse(xhr.responseText);

                            if (!json || typeof json.location != 'string') {
                                console.log("hihi");

                                failure('Invalid JSON: ' + xhr.responseText);
                                return;
                            }
                            console.log("urlll ", json.location);
                            success(json.location);
                        };

                        xhr.onerror = function () {
                            failure('Image upload failed due to a XHR Transport error. Code: ' + xhr.status);
                        };

                        formData = new FormData();
                        formData.append('file', blobInfo.blob(), blobInfo.filename());

                        xhr.send(formData);
                    },

                    toolbar: 'undo redo | formatselect | ' +
                        'bold italic backcolor | alignleft aligncenter ' +
                        'alignright alignjustify | bullist numlist outdent indent | ' +
                        'removeformat | image | help',
                    content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
                }}
            />
            <button onClick={postDocumentHandle}>Tạo Mới</button>
            <button onClick={putDocumentHandle}>Update</button>
        </div>
    );
}

export default TinyComponent;
