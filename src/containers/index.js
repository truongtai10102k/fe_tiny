import React, { useEffect } from 'react';
import MainComponent from '../components/MainComponent';
import * as actions from '../actions/index'
import { connect } from 'react-redux';
const TinyContainer = (props) => {
    //component did mount
    
    useEffect(() => {
        props.getDocument()
    }, []);

    return (
        <div>
            <MainComponent {...props} />
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        listItems : state.document.listItems
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getDocument: (data) => {
            dispatch(actions.getDocument(data))
        },
        postDocument: (data) => {
            dispatch(actions.postDocument(data))
        },
        putDocument: (data) => {
            dispatch(actions.putDocument(data))
        },
        deleteDocument: (data) => {
            dispatch(actions.deleteDocument(data))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TinyContainer);
