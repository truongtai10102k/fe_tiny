// domain
export const DOMAIN = "http://localhost:3001/api/editor";
// export const DOMAIN = "https://f32531d8ff9a.ngrok.io";
export const LIMIT = 10;

// HTTP method 
export const HTTP_READ = "GET"
export const HTTP_CREATE = "POST"
export const HTTP_UPDATE = "PUT"
export const HTTP_DELETE = "DELETE"

// HTTP headers
export const HTTP_HEADER_JSON = { "Content-Type": "Application/json" }

// types of actions
export const GET_DOCUMENT_REQUEST = "GET_DOCUMENT_REQUEST"
export const GET_DOCUMENT_SUCCESS = "GET_DOCUMENT_SUCCESS"
export const GET_DOCUMENT_FAILURE = "GET_DOCUMENT_FALIURE"

export const POST_DOCUMENT_REQUEST = "POST_DOCUMENT_REQUEST"
export const POST_DOCUMENT_SUCCESS = "POST_DOCUMENT_SUCCESS"
export const POST_DOCUMENT_FAILURE = "POST_DOCUMENT_FALIURE"

export const PUT_DOCUMENT_REQUEST = "PUT_DOCUMENT_REQUEST"
export const PUT_DOCUMENT_SUCCESS = "PUT_DOCUMENT_SUCCESS"
export const PUT_DOCUMENT_FAILURE = "PUT_DOCUMENT_FALIURE"

export const DELETE_DOCUMENT_REQUEST = "DELETE_DOCUMENT_REQUEST"
export const DELETE_DOCUMENT_SUCCESS = "DELETE_DOCUMENT_SUCCESS"
export const DELETE_DOCUMENT_FAILURE = "DELETE_DOCUMENT_FALIURE"
