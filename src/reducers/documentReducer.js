import * as types from '../constants'
const DEFAULT_STATE = {
    listItems: [],
    isFetching: false,
    dataFetched: false,
    error: false,
    errorMessage: null
}

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case types.GET_DOCUMENT_REQUEST:
            return {
                ...state,
                isFetching: true
            }
        case types.GET_DOCUMENT_SUCCESS:
            return {
                ...state,
                isFetching: false,
                dataFetched: true,
                listItems: action.payload
            }
        case types.GET_DOCUMENT_FAILURE:
            return {
                ...state,
                isFetching: false,
                error : true,
                errorMessage : action.payload.message
            }
        case types.POST_DOCUMENT_REQUEST:
            return {
                ...state,
                isFetching: true
            }
        case types.POST_DOCUMENT_SUCCESS:
            return {
                ...state,
                isFetching: false,
                dataFetched: true,
            }
        case types.POST_DOCUMENT_FAILURE:
            return {
                ...state,
                isFetching: false,
                error : true,
                errorMessage : action.payload.message
            }
        case types.PUT_DOCUMENT_REQUEST:
            return {
                ...state,
                isFetching: true
            }
        case types.PUT_DOCUMENT_SUCCESS:
            return {
                ...state,
                isFetching: false,
                dataFetched: true,
            }
        case types.PUT_DOCUMENT_FAILURE:
            return {
                ...state,
                isFetching: false,
                error : true,
                errorMessage : action.payload.message
            }
        case types.DELETE_DOCUMENT_REQUEST:
            return {
                ...state,
                isFetching: true
            }
        case types.DELETE_DOCUMENT_SUCCESS:
            return {
                ...state,
                isFetching: false,
                dataFetched: true,
            }
        case types.DELETE_DOCUMENT_FAILURE:
            return {
                ...state,
                isFetching: false,
                error : true,
                errorMessage : action.payload.message
            }

        default:
            return {
                ...state
            }
    }
}