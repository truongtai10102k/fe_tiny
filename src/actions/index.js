import * as types from '../constants'

export const getDocument = (payload) => {
    return ({
        type: types.GET_DOCUMENT_REQUEST,
        payload
    })
}

export const postDocument = (payload) => {
    return ({
        type: types.POST_DOCUMENT_REQUEST,
        payload
    })
}

export const putDocument = (payload) => {
    return ({
        type: types.PUT_DOCUMENT_REQUEST,
        payload
    })
}

export const deleteDocument = (payload) => {
    return ({
        type: types.DELETE_DOCUMENT_REQUEST,
        payload
    })
}