import React, { Component } from 'react';
import TinyContainer from '../containers';
class HomePage extends Component {
  render() {
    return (
      <div className="HomePage">
        <TinyContainer />
      </div>
    );
  }
}

export default HomePage;
